package interview.code.sampletask.template

import interview.code.sampletask.dto.BinanceKline
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class BinanceTemplate() {
    val URL = "https://api.binance.com/api/v3"
    private val restTemplate: RestTemplate = RestTemplate()
    fun getValuesBySymbolAndString(symbol: String, interval: String, limit: Int): List<BinanceKline>? {
        return restTemplate.getForObject(
            "$URL/klines",
            List::class.java,
            symbol,
            interval,
            limit
        ) as List<BinanceKline>?
    }
}