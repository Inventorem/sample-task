package interview.code.sampletask.service

import interview.code.sampletask.dto.GetVolatilityResponse
import interview.code.sampletask.template.BinanceTemplate
import org.springframework.stereotype.Service

@Service
class BinanceVolatilityCalcService(
    private val template: BinanceTemplate
) : VolatilityCalcService {
    override fun getVolatilityBySymbolAndInterval(symbol: String, interval: String, limit: Int): GetVolatilityResponse {
        val klines = template.getValuesBySymbolAndString(symbol, interval, limit) ?: throw Exception("Not found")
        val maxHigh = klines.map { it.high }.max()
        val minLow = klines.map { it.low }.min()
        val volatility = (maxHigh - minLow) / minLow * 100
        return GetVolatilityResponse(symbol, interval, volatility)
    }


}