package interview.code.sampletask.service

import interview.code.sampletask.dto.GetVolatilityResponse

interface VolatilityCalcService {

    fun getVolatilityBySymbolAndInterval(symbol: String, interval: String, limit: Int): GetVolatilityResponse =
        throw UnsupportedOperationException()

}