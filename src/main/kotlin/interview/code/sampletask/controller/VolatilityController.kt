package interview.code.sampletask.controller

import interview.code.sampletask.dto.GetVolatilityRequest
import interview.code.sampletask.dto.GetVolatilityResponse
import interview.code.sampletask.service.VolatilityCalcService
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/volatility")
class VolatilityController(
    private val volatilityCalcService: VolatilityCalcService
) {

    @Value("\${interview.symbols}")
    private lateinit var interviewSymbols: List<String>

    @GetMapping("/calculate")
    fun calculateByPair(@RequestBody request: GetVolatilityRequest): GetVolatilityResponse {
        return volatilityCalcService.getVolatilityBySymbolAndInterval(request.symbol, request.interval, request.limit)
    }

    @GetMapping("/calculate")
    fun calculateByPairs(@RequestBody requests: List<GetVolatilityRequest>): List<GetVolatilityResponse> {
        return requests.map {
            volatilityCalcService.getVolatilityBySymbolAndInterval(it.symbol, it.interval, it.limit)
        }
    }

    @GetMapping("/calculate-hour")
    fun calculateSymbolByHour(symbol: String): GetVolatilityResponse {
        return volatilityCalcService.getVolatilityBySymbolAndInterval(symbol, "1m", 60)
    }

    @GetMapping("/calculate-hour")
    fun calculateSymbolsByHour(symbols: List<String>): List<GetVolatilityResponse> {
        return symbols.map { volatilityCalcService.getVolatilityBySymbolAndInterval(it, "1m", 60) }
    }

    @GetMapping("/task")
    fun calculateForTaskSymbols() =
        interviewSymbols.map { volatilityCalcService.getVolatilityBySymbolAndInterval(it, "1m", 60) }
}