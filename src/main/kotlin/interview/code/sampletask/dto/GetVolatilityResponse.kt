package interview.code.sampletask.dto

data class GetVolatilityResponse(
    val symbol: String,
    val interval: String,
    val volatility: Double
)
