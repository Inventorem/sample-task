package interview.code.sampletask.dto

data class GetVolatilityRequest(val symbol: String, val interval: String, val limit: Int)