package interview.code.sampletask.dto

data class BinanceKline(
    val high: Double,
    val low: Double
)

